# flexible-CV



***


## Name
flexible-CV template for pdfLATeX XeLaTeX or LuaLaTeX

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.


## Installation
Nothing different than usual LaTeX packages usage.

## Usage
I will fix the readme file soon, for now you may play with the parameters that are and that aren't commented in the main file

## Support
You may donate via paypal to koleygr@gmail.com
Actually I am in need of money for survival reasons till I have much expences due to recent move in Athens.
In the future I hope I will remove this support message and will ask from you to donate to other people in need (possibly of your preference)


## Authors and acknowledgment
Konstantinos Leledakis: feel free to mail to koleygr@gmail.com

## License
flexible-CV project is lisenced under these terms:

1. You should always be available to share the code or any edit that you have made in the template (So, this project and its derivatives will remain always open source)
2. You may use it without any charge for personal reasons.
3. If you use it for any kind of commersial reason, and if you have the ability to do so, you have to donate to someone (not nesesserily the creator) that has survival needs of money or of your donation in any way.
4. This lisence has to be used in any derivative work of this original project with an active link to this project too inside the lisence.

## Project status
This is the version 1.00 (not extended tested, but close to an alpha release)
